//
//  ViewController.swift
//  SwiftDemo_PL1
//
//  Created by formando on 05/11/15.
//  Copyright © 2015 pt.ipleiria. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    var johnDoe:Person = Person()
    var michaelSmith:Person = Person(name: "Michael", lastName: "Smith", nationality: "English")
    
    
    struct imcMeasures {
        var height = 0.0
        var weight = 0.0
    }
    
    enum CalculatorOperation {
        case Sum
        case Subtration
        case Multiplication
        case Division
    
        func description -> String{
            switch self {
            case .Sum:
                return "Sum"
            case .Subtration:
                return "Subtration"
            case .Multiplication:
                return "Multiplication"
            default:
                return "Division"
            }
        }
    }
    
    enum Result{
        case Sucess (Int, Int)
        case Failure (String)
    }
    
    var correuBem = Result.Sucess(15, 17)
    var correuMAl = Result.Failure("Timeout")
    //var ComoCorreu = correuBem
   
    
    var op1:CalculatorOperation = .Subtration
    
    var johnDoeMeasures = imcMeasures(height: 1.85, weight:85)
    
    
    @IBAction func buttonPressed(sender: UIButton) {
        if let fullname = johnDoe.fullName(){
            label.text = fullname
        }
        
        label.text = "John Doe is \(johnDoeMeasures.height)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        johnDoe.name = "John"
        johnDoe.lastName = "Doe"
        johnDoe.nationality = "Turkish"
        
        michaelSmith.nationality = "American"
        
        
        switch (correuBem){
        case let .Sucess(x,y):
            print("x: \(x) y:\(y)")
        case .Failure (let str):
            print("Error: \(str)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
}

