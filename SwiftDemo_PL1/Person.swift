//
//  Person.swift
//  SwiftDemo_PL1
//
//  Created by formando on 09/11/15.
//  Copyright © 2015 pt.ipleiria. All rights reserved.
//

import Foundation

class Person {
    var name:String?
    var lastName:String?
    var nationality:String?
    
    init(){
        nationality = "Portuguese"
    }
    
    init(name:String, lastName:String, nationality:String){
        self.name = name
        self.lastName = lastName
        self.nationality = nationality
    }
    
    func fullName() -> String?{
        if let name = self.name{
            if let lastName = self.lastName{
                return name + " " + lastName
            }
        }
        return nil
    }
}